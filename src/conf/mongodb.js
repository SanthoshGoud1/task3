const { MongoClient } = require("mongodb");
const config = require('./app.spec.json');
const { Util } = require('../../common/util');
const util = new Util();
let Mongoconnection
async function dbConnection() {             //connecting Db
    try {
        const client = new MongoClient(config.db.url);
        Mongoconnection = await client.connect();
        const _db = Mongoconnection.db(config.db.dbName);
        console.log("connected..");
        return _db;
    }
    catch (error) {
        console.error(error);
    }
}
async function listRec(params) {    //list of records in Db
   
    return new Promise(async (resolve, reject) => {
        try {
            const { rectype, ...restParams } = params
            const db = await dbConnection()
            const getDetails = await db.collection(rectype).find(restParams).toArray()
           resolve(getDetails)
        }
        catch (error) {
            reject(error)
        }
        finally {
            console.log("Connection Closed");
            await Mongoconnection.close();
        }

    });
}
async function updateRec(params) {    //updating Db
    return new Promise(async (resolve, reject) => {
        try {
            const { rectype, id,body } = params
            console.log(rectype,params);

            const updatingDetails = await dbConnection()
            const uptodate = await updatingDetails.collection(rectype).updateOne({ id: id },{ $set: body})
            resolve(uptodate)
        }
        catch (error) {
            reject(error);
        }
        finally {
            console.log("Connection Closed");
            await Mongoconnection.close();
        }
    });
}


async function delRec(params) { //deleting thr record in db
    return new Promise(async (resolve, reject) => {
        try {
            const { rectype, id } = params
            const db = await dbConnection() 
            const result = await db.collection(rectype).deleteOne({id}); //find and delete the selected id
            if (!result.deletedCount) {
              throw `${rectype} Record is Not Found!`;
            }
            resolve(`${rectype} record is successfully deleted!`);
        }
        catch (error) {
            reject(error);
        }
        finally {
            console.log("Connection Closed");
            await Mongoconnection.close();
        }
    })
}
//generating sequencial id 
async function getSequence(db) {
    const value = await db.collection(config.collectonSequence)
        .findOneAndUpdate({ id: "0" },
            { $inc: { 'data': 1 } },
            { new: true })

    return value.value.data.toString()
}
async function addRec(params) {//adding Record in Db
    return new Promise(async (resolve, reject) => {
        try {

            const { rectype } = params
            const db = await dbConnection()
            params.id = await getSequence(db)
            params.creted = util.dateFunction()
            
            const resultRec = await db.collection(rectype).insertOne(params);
            resolve(params)
        }
        catch (error) {
            reject(error);
        }
        finally {
            console.log("Connection Closed");
            await Mongoconnection.close();
        }
    })
}
module.exports = { listRec, updateRec, delRec, addRec }