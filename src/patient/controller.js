const config = require("../conf/app.spec.json");
const { listRec, updateRec, delRec, addRec } = require("../conf/mongodb");

const { Util } = require("../../common/util");

const utils = new Util();

//createRec function used to call createRecord from mongodb file and get inserted response or error
async function createRec(req, res) {
  try {
    const {
      body: { orgid, dob },
    } = req;
    const orgParams = { rectype: config.organization.rectype, id: orgid, status: config.common.status.active };
    const orgData = await listRec(orgParams);
    if (!orgData.length) throw "Invalid/Inactive record!";
    utils.dateFormat(dob);
    req.body.rectype = config.patient.rectype;
    req.body.createdBy=req.session.params.id;
    const patientInfo = await addRec(req.body);
    res.status(200).json({ status: "Success", results: patientInfo });
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error});
  }
}

//getRec function used to call getRecord from mongodb file and get record response or error
async function getRec(req, res) {
  try {
    const { query } = req;
    const payload = query;
    payload.rectype = config.patient.rectype;
    const patientInfo = await listRec(payload);

    res.status(200).json({ status: "Success", results: patientInfo });
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error });
  }
}

//updateRec function used to call updateRecord from mongodb file and get updated response or error
async function update_Rec(req, res) {
  try {
    const { query } = req;
    const payload = query;
    payload.rectype = config.patient.rectype;
    payload.body = req.body;
    
    //check if status is inactive or not, if inactive then check inactivereason and dateinactivate
    if (req.body.status == config.common.status.inactive) {
      if (req.body.inactivereason)
        req.body.dateinactivate = utils.getCurrentDateTime();
      else throw "Enter inactivereason!";
    }
    
    const patientInfo = await updateRec(payload);
    res.status(200).json({ status: "Success", results: patientInfo });
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error });
  }
}

//deleteRec function used to call deleteRecord from mongodb file and get deleted response or error
async function deleteRec(req, res) {
  try {
    const { query } = req;
    const payload = query;

    payload.rectype = config.patient.rectype;
    const patientInfo = await delRec(payload);
    res.status(200).json({ status: "Success", results: patientInfo });
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error });
  }
}

//export all functions
module.exports = { createRec, update_Rec, deleteRec, getRec }