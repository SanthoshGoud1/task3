const {listRec, updateRec, delRec, addRec} = require('../conf/mongodb')
const config= require("../conf/app.spec.json")
const {Util}=require("../../common/util")
const util=new Util();
//Post Data
async function createRec(req, res) {
  try {
    req.body.rectype = config.organization.rectype
    const postDetails = await addRec(req.body)
    res.status(200).json({ status: "success", result: postDetails })
  }
  catch (error) {
    res.status(400).json({ status: "error", error: error })
  }
}
//Get data
async function getRec(req, res) {
  try {
    const { query } = req;
    const payload = query;
    payload.rectype = config.organization.rectype;

    const data = await listRec(payload);
    res.status(200).json({ status: "Success", results: data });
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error });
  }
}
//put data
async function update_Rec(req, res) {
  try {
    const { query } = req;
    const payload = query;
    payload.rectype = config.organization.rectype;
    payload.body = req.body;
    //check if status is inactive or not, if inactive then check inactivereason and dateinactivate
    if (req.body.status == config.common.status.inactive) {
      if (req.body.inactivereason)
        req.body.dateinactivate = util.dateFunction();
     else 
      throw "Enter inactivereason!";
    }

    const data = await updateRec(payload);
    res.status(200).json({ status: "Success", results: data });
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error });
  }
}
//deleteDetails
async function deleteRec(req, res) {
  try{
    const { query } = req;
    const params = query;
    params.rectype = config.organization.rectype;
  const deleteRecord = await delRec(params)
  res.status(200).json({status:"success",result:deleteRecord})
  }
  catch(error){
    res.status(400).json({status:"error",error:error})
  }
}

module.exports = { createRec, update_Rec, deleteRec, getRec }