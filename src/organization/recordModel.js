const Schema = require("validate");
const config = require("../conf/app.spec.json");
const {addRec}=require('../../src/conf/mongodb')
//schema for recodeModule
const recordModelSchema = new Schema({
  id: { type: String },
  rectype: { type: String },
  orgid: { type: String }, // patientorgid 
  refrectype: {
    type: String,
    enum: [config.patient.rectype],
  },
  type: {
    type: String,
    enum: Object.values(config.common.type),
    required: true,
  },
  data: {
  },
});

//validation function is used to validate requested fields with the schema
function validation(validateParams) {
  //validate the schema with requested data
  let errors = recordModelSchema.validate(validateParams);
  if (errors.length) {
    errors = errors.map((eRec) => {
      return { path: eRec.path, message: eRec.message };
    });
    throw errors[0].message;
  } else {
    return true;
  }
}

//creating recordeModel
async function createRecModel(req, res) {
  try {
    const {
     orgid,type,data 
    } = req.body;
    const Params = {type,orgid,data};
  validation(Params)
  req.body.rectype=config.recordmodel.rectype
  req.body.refrectype=config.refrectype.patient
  const record = await addRec(req.body);
    res.status(200).json({ status: "Success", results: record});
    
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error});
  }
}
module.exports={createRecModel}