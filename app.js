const express=require('express')
const bodyParser=require("body-parser")

const app=express()
app.use(bodyParser.json())


app.set("view engine", ".hbs");
app.use("/", express.static(__dirname + '/common/pdfGenerator/template.hbs'));

const orgRoutes=require('./routes/organization')
const patRoutes=require("./routes/patient")
const fileRoutes=require("./routes/file")
const recordmodel=require('./routes/organization')
const userRoutes=require("./routes/user")

app.use("/organization",orgRoutes) 
app.use("/patient",patRoutes)
app.use("/file",fileRoutes)
app.use("/recordmodel",recordmodel)
app.use("/user",userRoutes)
const PORT = process.env.PORT || 8008;
app.listen(PORT, console.log("Server don start for port: " + PORT))
