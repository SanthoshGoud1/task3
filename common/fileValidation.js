const Schema = require('validate')
const config=require('../src/conf/app.spec.json')
function validation_file(file_schema) {
    const user = new Schema({
        id: {
            type: String,
        },
        rectype: {
            type: String,
        },
        refid: {
            type: String,
        },
        refrectype: {
            type: String,
            enum:Object.values(config.refrectype)
        },
        orgid: {
            type: String,
        },
        status: {
            type: String,
            enum:Object.values(config.file_status)
        },
        url: {
            type: String,
        },
        name: {
            type: String,
        },
        originalname: {
            type: String,
        },
        type: {
            type: String
        },
        size: {
            type:Number
        },
        created: {
            type:String
        },
        data:{
            type:String
        }
    },
    )
   const error=user.validate(file_schema);
   return error
  }
  module.exports=validation_file