const config = require("../src/conf/app.spec.json")
const Schema = require("validate")
const { addRec, listRec,delRec } = require("../src/conf/mongodb");

const authSchema = new Schema({
    refid: { type: String },
    rectype: { type: String },
    token: { type: String }
});
function validation(validateParams) {
    //validate the schema with requested data
    let errors = authSchema.validate(validateParams);
    if (errors.length) {
        errors = errors.map((eRec) => {
            return { path: eRec.path, message: eRec.message };
        });
        throw errors[0].message;
    } else {
        return true;
    }
}

async function addToken(params) {
    params.rectype = config.token.rectype
    const {rectype,refid }=params
    validation(params)
    const tokenRec = await listRec({rectype,refid })
    if (!tokenRec.length)//If record is empty
    {
        await addRec(params)
        return { message: "Token Generated" }
    }
    else {
        const { id } = tokenRec[0]
        await delRec({rectype,id }) //remove if any old record exists for this user
        await addRec(params) // create token record
        return { message: "Token Genereted" }
    }
}

async function deleteToken(params) {
        try {
          return await delRec(params)
        } catch (error) {
          return error
        }
      
}

module.exports = { addToken, deleteToken }