const puppeteer = require('puppeteer')
const fs = require('fs')
const path = require("path");
const { listRec } = require('../../src/conf/mongodb')
const config = require('../../src/conf/app.spec.json')
const { Util } = require("../util.js")
const util = new Util();
const datetoAge = require("dob-to-age")
const hbs = require('handlebars');
const axios = require('axios')

const compile = function (templateName, data) {
  const filePath = path.join(process.cwd(), '/common/pdfGenerator', `${templateName}.hbs`);
  const html = fs.readFileSync(filePath, 'utf8')
  return hbs.compile(html)(data);
};
function puppeteerFunction(data) {
  return new Promise(async (resolve, reject) => {
    const browser = await puppeteer.launch();
    try {

      const page = await browser.newPage();

      const content = await compile('template', data);
      const pdfName = `${data.firstname}` + '_' +
        `${data.lastname}` + '_' + `${data.rectype}` + '_' + 'Info' + ".pdf"

      await page.setContent(content);

      await page.pdf({
        path: pdfName,
        format: 'A4',
        printBackground: true
      })

      resolve("ok")
    } catch (error) {
      reject(error)
    }
    finally {
      await browser.close();
      process.exit();
    }
  })

}
function patientData(req, res) {
  new Promise(async () => {
    try {
      const { id } = req.body
      const params = { id, rectype: config.patient.rectype }
      const patientInfo = await listRec(params)
      const { firstname, lastname, gender, dob, orgid, mobile, email, rectype } = patientInfo[0]
      const contactInfoGet = { refid: id, rectype: config.contact.rectype }
      const contactInfo = await listRec(contactInfoGet)
      pdfData = {
        orgid, firstname, lastname, gender, dob, address: contactInfo[0].address,
        date: util.dateFormat(),
        age: datetoAge(dob),
        mobile, email, rectype,imgUrl:"https://www..com/hubfs/Innominds-201612/img/nav/Innominds-Logo.png"
      }
      const list = await puppeteerFunction(pdfData);

      res.status(200).json({ status: "Success", results: "Generated Pdf" });
      console.log(list);
    }
    catch (error) {
      res.status(400).json({ status: "Error :", error: error });
    }
  })
}
module.exports = { patientData }