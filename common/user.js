const Schema = require("validate");
const config = require("../src/conf/app.spec.json");

const {
  addRec,
  updateRec,
  listRec,
  delRec,
} = require("../src/conf/mongodb");

const userSchema = new Schema({
    id: { type: String },
    rectype: { type: String },
    orgid: { type: String },
    firstname: {
      type: String,
    },
    lastname: { type: String},
    gender: {
      type: String,
      enum: Object.values(config.common.gender),
      required: true,
    },
    dob: {
      type: String,
      required: true,
    },
    status: {
        type: String,
        enum: Object.values(config.common.status),
        required: true,
      },
      created: {
        type: String,
      },
      data:{

      }
  });
  function validation(validateParams) {
    //validate the schema with requested data
    let errors = userSchema.validate(validateParams);
    if (errors.length) {
      errors = errors.map((eRec) => {
        return { path: eRec.path, message: eRec.message };
      });
      throw errors[0].message;
    } else {
      return true;
    }
  }
  async function createRec(req, res) {
    try {
        validation(req.body)
        req.body.rectype=config.user.rectype
        await addRec(req.body);
      res.status(200).json({ status: "Success", results: req.body });
    } catch (error) {
      res.status(400).json({ status: "Error :", error: error});
    }
  }
  
  //getRec function used to call getRecord from mongodb file and get record response or error
  async function getRec(req, res) {
    try {
      const { query } = req;
      const payload = query;
      payload.rectype = config.user.rectype;
      const userInfo = await listRec(payload);
      res.status(200).json({ status: "Success", results: userInfo});
    } catch (error) {
      res.status(400).json({ status: "Error :", error: error });
    }
  }
  
  //updateRec function used to call updateRecord from mongodb file and get updated response or error
  async function update_Rec(req, res) {
    try {
      const { query } = req;
      const payload = query;
      payload.rectype = config.user.rectype;
      payload.body = req.body;
      
      //check if status is inactive or not, if inactive then check inactivereason and dateinactivate
      if (req.body.status == config.common.status.inactive) {
        if (req.body.inactivereason)
          req.body.dateinactivate = utils.getCurrentDateTime();
        else throw "Enter inactivereason!";
      }
      
      const patientInfo = await updateRec(payload);
      res.status(200).json({ status: "Success", results: patientInfo });
    } catch (error) {
      res.status(400).json({ status: "Error :", error: error });
    }
  }
  
  //deleteRec function used to call deleteRecord from mongodb file and get deleted response or error
  async function deleteRec(req, res) {
    try {
      const { query } = req;
      const payload = query;
  
      payload.rectype = config.user.rectype;
      const patientInfo = await delRec(payload);
      res.status(200).json({ status: "Success", results: patientInfo });
    } catch (error) {
      res.status(400).json({ status: "Error :", error: error });
    }
  }
  
  //export all functions
  module.exports = { createRec, update_Rec, deleteRec, getRec }
