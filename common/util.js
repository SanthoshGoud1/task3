const config = require("../src/conf/app.spec.json");
const emailvalidator = require("email-validator");
const validatefax = new RegExp(config.contact.faxregex);
const validatephone=new RegExp(config.contact.phonenumberreqex)
const moment=require("moment")
const JWT = require("jsonwebtoken");
const createError = require('http-errors');
class Util {
    dateFunction() {
        const date = new Date()
        return date
    };
    dateFormat()
    {
      const dateformat=this.dateFunction()
      return moment(dateformat).format('MM/DD/YYYY, h:mm:ss a');
    }
   
    async getorgid(params)
    {
        const {listRec}= require("../src/conf/mongodb")

        const record =await listRec(params)
       return record[0].orgid;
    }
    validateAddress(params) {
        const { address, checkaddress } = params;
        checkaddress.forEach((element) => {
          if (!address.hasOwnProperty(element))
            throw "address mustbe in a format like line1, line2, city, state, zip";
        });
        return true;
      }
      validateEmail(email) {
        if (!emailvalidator.validate(email)) throw "Enter Valid email Id!";
        else return true;
      }
    
      //validateFax function is used to validate the fax
      validateFax(fax) {
        if (!validatefax.test(fax)) throw "Enter Valid Fax!";
        else return true;
      }
      validatePhone(phone) {
        if (!validatephone.test(phone)) throw "Enter Valid Fax!";
        else return true;
      }
      hasSameKeys( recordData, recordmodelData ){

        let empObj={}
        Object.keys(recordmodelData).forEach((element)=>{
          if(recordData.hasOwnProperty(element))
          {
           empObj[element]=recordData[element]
          }
        })``
        return empObj
        }

        jwtTokenGeneration(params)
        {
          return JWT.sign({params},config.SecretAccess_Key,{expiresIn:'120m'})
        }
        jwtTokenVerification(params)
        {
         const tokendata= JWT.verify(params, config.SecretAccess_Key, (err, payload) => {
            if (err) {
                  throw err
            }
                return  payload
        }) 
        return tokendata
        }

}
module.exports = {Util}