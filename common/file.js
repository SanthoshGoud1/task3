const validation_file=require('./fileValidation')
const dbConnection=require('../src/conf/mongodb')
const s3Uplode=require('./s3Bucket')

async function addFile(req,res)
{
    try{
    const file=req.files
    const paramas=req.body
            const {originalname,mimetype:type,size}=file[0]
            const name=originalname.slice(0, -5)
         const result=await s3Uplode(file[0])
         const {Location:url}=result
            const request={rectype:paramas.rectype,
                refid:paramas.refid,
                refrectype:name,
                url,originalname,name
                ,type,size
            }
            
    const validate_result=validation_file(request)
    console.log(validate_result);
    if(validate_result.length==0)
    {
       const postDetails=await dbConnection.addRec(request) 
        res.send(postDetails)
         
    }
   else{
        res.send("error in validation")
   }
    }
    catch(e)
    {
        console.log(e);
    }
       
}

async function deleteFile(req, res) {
    const paramas={id:req.params.id,collection:"'file'"}
    const deleteRec=await dbConnection.delRec(paramas)
    res.send(deleteRec)
}

module.exports={addFile,deleteFile}

