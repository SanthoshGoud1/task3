const Schema = require("validate");
const { addToken} = require("../common/token")
const { Util } = require("../common/util");
const util = new Util();
const config = require("../src/conf/app.spec.json")

const md5 = require("md5")
const {
  addRec,
  listRec,
  delRec,
} = require("../src/conf/mongodb");
const authSchema = new Schema({
  id: { type: String },
  rectype: { type: String },
  orgid: { type: String },
  refrectype: { type: String },
  refid: { type: String },
  data: {
    username: { type: String },
    password: { type: String }
  }
});
function validation(validateParams) {//validate the schema with requested data
  let errors = authSchema.validate(validateParams);
  if (errors.length) {
    errors = errors.map((eRec) => {
      return { path: eRec.path, message: eRec.message };
    });
    throw errors[0].message;
  } else {
    return true;
  }
}
async function setAuth(params) {
  try {
    validation(params)
    params.data.password = md5(params.data.password)
    const authRec = await listRec({ rectype: params.rectype, refid: params.refid })
    params.refrectype = config.user.rectype
    if (!authRec.length)//If empty record is empty
    {
      return await addRec(params)
    }
    else {
      const { id } = authRec[0]
      await delRec({ rectype: params.rectype, id })//remove if any old record exists for this user
      return await addRec(params)// create new authentication record
    }
  }
  catch (error) {
    return error
  }
}
async function validateAuth(params) {
  try {
    params.password = md5(params.password)
    const { username, password, rectype } = params
    const authdata = await listRec({ rectype, data: { username, password } })
    if (!authdata.length) throw "user details not found"
    const { refrectype, refid } = authdata[0];
    const userparams = { rectype: refrectype, id: refid, status: config.common.status.active }
    const userdata = await listRec(userparams)
    if (!userdata.length) throw "user is inactive"
    const { id, orgid, firstname, lastname } = userdata[0]
    const token = util.jwtTokenGeneration({ id, orgid, firstname, lastname, username })
    const tokenparams = { token, refrectype, refid }
    await addToken(tokenparams)
    return token
  }
  catch (error) {
    return error
  }
}
module.exports = {
  setAuth, validateAuth
};
