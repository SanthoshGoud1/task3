const Schema = require("validate");
const config = require("../src/conf/app.spec.json");
const {addRec,listRec}=require('../src/conf/mongodb');
const {Util}=require("../common/util")
const util=new Util()
//schema for recodeModule
const recordSchema = new Schema({
  rectype: { type: String },
  orgid: { type: String }, // patientorgid 
  refrectype: {
    type: String,
    enum: [config.patient.rectype],
  },
  recordmodelid: {
     type: String 
    },
  type: {
    type: String,
    enum: Object.values(config.common.type),
    required: true,
  },
});

//validation function is used to validate requested fields with the schema
function validation(validateParams) {
  //validate the schema with requested data
  let errors = recordSchema.validate(validateParams);
  if (errors.length) {
    errors = errors.map((eRec) => {
      return { path: eRec.path, message: eRec.message };
    });
    throw errors[0].message;
  } else {
    return true;
  }
}

//creating recordeModel
async function createRecord(req, res) {
  try {
    const {
     orgid,recordmodelid,type,data 
    } = req.body;
    const Params = {type,recordmodelid,orgid,data};
  validation(Params)
  Params.rectype=config.record.rectype
  Params.refrectype=config.refrectype.patient
  const findRec={rectype:config.recordmodel.rectype,id:recordmodelid}
  const recodemodel=await listRec(findRec)
  const comparingObj=util.hasSameKeys(req.body.data,recodemodel[0].data)
  //inserting data into collection
       Params.data=comparingObj
   const record=await addRec(Params)
 
    res.status(200).json({ status: "Success", results: record});
    
  } catch (error) {
    res.status(400).json({ status: "Error :", error: error});
  }
}
module.exports={createRecord}