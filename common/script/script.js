const fetch = require("node-fetch");
const config = require("../../src/conf/app.spec.json")
const axios = require('axios');
async function getRandomUsers(count) {
    const arr = []
    const randomuser = await fetch('https://randomuser.me/api/?results=' + count)
    var jsonData = await randomuser.json();

    jsonData.results.forEach(({ gender, name: { first, last }, email, phone, dob: { date, age },
        location: { street: { number, name }, city, state, postcode } } = jsonData.results) => {
        const dateUpdate = new Date(date);
        const dob = dateUpdate.getFullYear() + '-' + (dateUpdate.getMonth() + 1) + '-' + dateUpdate.getDate();
        const address = { line1: number, line2: name, city: city, state: state, zip: postcode }
        const obj = {
            patient: { firstname: first, lastname: last, gender, dob, age },
            contact: { email, phone, address }
        }
        arr.push(obj)
    });
    return arr
};

async function getToken() {
    const credentials = {
        username: "santhosh",
        password: "Asdrthdr1@"
    }
    const tokenUrl = "http://localhost:8008/user/login"
    const tokenresponse = await axios.post(tokenUrl, credentials)
        .then(function (response) {
            return response
        })
        .catch(function (error) {
            console.log(error);
        });
    return tokenresponse
}
async function getOrgId(headers) {
    const orglink = "http://localhost:8008/organization/get"
    const orgobject = { rectype: config.organization.rectype }
    const orgid = await axios.get(orglink, orgobject, { headers: headers })
    const data=orgid.data.results
    console.log("hello");
    let Result = data.map(choice => ({ id:choice.id }));
    
}
async function createPatient(patientObject, token) {
    const patienturl = "http://localhost:8008/patient/create"
    const headers = {
        Authorization: `Bearer ${token}`
    }
    patientObject.status = config.common.status.active
    patientObject.orgid = getOrgId(headers)
  // const patientData = await axios.post(patienturl, patientObject, { headers: headers })
   //console.log(patientData.data.results);

}
function createContact(contactObject, patientId, token) {

}

function processFunction(randomuser, token) {
    const patientInfo = createPatient(randomuser.patient, token)
    //createContact(randomuser.contact, patientInfo.id, token)
}

async function start(count) {
    const randomuser = await getRandomUsers(count)
    var token = await getToken()
    token=token.data.results
    randomuser.map((record) => {
        processFunction(record, token)
    });

}
start(5)