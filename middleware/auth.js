const createError = require('http-errors');
const {Util}=require('../common/util');
const {listRec}=require('../src/conf/mongodb')
const util=new Util();
const config=require('../src/conf/app.spec.json')

const verifyAccessToken =async(req, res, next) => {
    if (!req.headers.authorization) return next(createError.Unauthorized())
    const authHeader = req.headers.authorization
    const bearerToken = authHeader.split(' ')
    const token = bearerToken[1]
    const tokendata=util.jwtTokenVerification(token)
    const{params:{id}}=tokendata
    const params={refid:id,rectype:config.token.rectype}
    const tokeninfo=await listRec(params)
    if(!tokeninfo.length) throw "token record not found"
    const {token:rectoken,refid}=tokeninfo[0]
    if(token!=rectoken) res.send("token is not Valid");
    const userparams={id:refid,rectype:config.user.rectype,status:config.common.status.active}
    const userdata=await listRec(userparams)
    if(!userdata.length) res.send("User is Inactive");
    req.session=tokendata
    next();
}
module.exports = { verifyAccessToken }
