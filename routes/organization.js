const express=require('express')
var router = express.Router();
const {getRec,createRec,update_Rec,deleteRec}=require('../src/organization/controller');
const {Validation}=require("../src/organization/organization.js")
const {processFun}=require("../src/contact/controller")

const config=require("../src/conf/app.spec.json")

router.get('/get',getRec)
router.post('/create',Validation,createRec)
router.put('/update',update_Rec)
router.delete('/delete',deleteRec)


const {createRecModel}=require("../src/organization/recordModel")
router.post('/post',createRecModel)

router.post('/contact', async(req, res)=>{
    try
    {
      const __action = req.body.__action;
      const processFunction = processFun(__action);
      const contactBody = req.body.body || {};
      contactBody.refrectype = config.organization.rectype;
      const result = await processFunction(contactBody);
      res.status(200).json({ status: "Success", results: result });
    } catch (error) {
      res.status(400).json({ status: "Error :", error: error });
    }
  });

module.exports=router