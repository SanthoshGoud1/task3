const express = require('express');
var router = express.Router();

const { getRec, createRec, update_Rec, deleteRec } = require('../src/patient/controller');
const { Validation } = require("../src/patient/patient")
const { patientData } = require("../common/pdfGenerator/pdf")
const { createRecord } = require("../common/records")
const { verifyAccessToken } = require("../middleware/auth")

router.get('/get', verifyAccessToken, getRec)

router.post('/create', verifyAccessToken, Validation, createRec)

router.put('/update', verifyAccessToken, update_Rec)

router.delete('/delete', verifyAccessToken, deleteRec)

router.get('/get/pdf', verifyAccessToken, patientData)

//Route for post data
router.post('/record', createRecord)

module.exports = router