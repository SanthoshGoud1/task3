const express=require('express')
var router = express.Router();
const {addFile,deleteFile}=require('../common/file')
const multer=require("multer")


var storage = multer.memoryStorage();
var uploadMem = multer({ storage: storage });

router.post('/common/file',uploadMem.array('file'),addFile)

router.delete('/common/file/:id',deleteFile)


module.exports=router