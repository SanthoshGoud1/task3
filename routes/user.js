const express = require('express');
var router = express.Router();
const config = require("../src/conf/app.spec.json")
const { verifyAccessToken } = require("../middleware/auth")

const { getRec, createRec, update_Rec, deleteRec } = require('../common/user');
const { setAuth, validateAuth } = require("../common/authentication")
const { deleteToken } = require("../common/token")


router.get('/get', getRec)
router.post('/create', createRec)
router.put('/update', update_Rec)
router.delete('/delete', deleteRec)


router.post('/setauth', async (req, res) => {
    try {
        req.body.rectype = config.authentication.rectype
        const finalRes = await setAuth(req.body)
        res.status(200).json({ status: "Success", results: finalRes });
    } catch (error) {
        res.status(400).json({ status: "Error :", error: error });
    }

})
router.post('/login', async (req, res) => {
    try {
        req.body.rectype = config.authentication.rectype
        const finalRes = await validateAuth(req.body)
        res.status(200).json({ status: "Success", results: finalRes });
    } catch (error) {
        res.status(400).json({ status: "Error :", error: error });
    }
})
router.delete('/logout', async (req, res) => {
    try {
        req.body.rectype = config.token.rectype
        const finalRes = await deleteToken(req.body)
        res.status(200).json({ status: "Success", results: finalRes });
    } catch (error) {
        res.status(400).json({ status: "Error :", error: error });
    }
})

module.exports = router